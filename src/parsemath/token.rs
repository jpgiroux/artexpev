#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Add,
    Subtract,
    Multiply,
    Divide,
    Caret,
    LeftParenthesis,
    RightParenthesis,
    Num(f64),
    EOF
}

#[derive(Debug, PartialOrd, PartialEq)]
pub enum OperPrec {
    DefaultZero,
    AddSub,
    MulDiv,
    Power,
    Negative
}

impl Token {
    pub fn get_oper_prec(&self) -> OperPrec {
        use self::Token::*;

        match *self {
            Add | Subtract => OperPrec::AddSub,
            Multiply | Divide => OperPrec::MulDiv,
            Caret => OperPrec::Power,
            _ => OperPrec::DefaultZero
        }
    }
}